//
//  ViewController.swift
//  FireAuth
//
//  Created by Faique Ali on 27/08/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {

    //Outlet
    @IBOutlet weak var emailOu: UITextField!
    @IBOutlet weak var PasswordOu: UITextField!
    
    @IBOutlet weak var phoneOu: UITextField!
    @IBOutlet weak var otpOu: UITextField!
    //Variables
    
    
    //Constants
    let userDefault = UserDefaults.standard  //small memory to safe data when app launchs
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //to hide keyboard when tapped around
        hideKeyboardWhenTappedAround()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        //if user already signed in, then take direct to SignInVc
        if userDefault.bool(forKey: "usersignedin") {
            performSegue(withIdentifier: "segueToSignin", sender: self)
        }
    }

    //Action
    @IBAction func signInButtonPressed(_ sender: Any) {
        signInUser(email: emailOu.text!, password: PasswordOu.text!)
    }
    
    //Helper Func
    func createUser(email: String, password: String){
        //firebase func to create a new user
        Auth.auth().createUser(withEmail: email, password: password) {(result, error) in
            if error == nil {
                //user created
                print("User created")
                //sign in user
                self.signInUser(email: email, password: password)
            } else {
                print(error?.localizedDescription)
            }
        }
    }
    
    func signInUser(email: String, password: String) {
        Auth.auth().signIn(withEmail: email, password: password) {(user, error) in
            if error == nil {
                //signed in
                print("User Signed In")
                
                //will synchronize(save) when user will log in
                self.userDefault.set(true,forKey: "usersignedin")
                self.userDefault.synchronize()
                
                //perform segue
                self.performSegue(withIdentifier: "segueToSignin", sender: self)
                
            } else if (error?._code == AuthErrorCode.userNotFound.rawValue){
                //print(error)
                //print(error?.localizedDescription)
                self.createUser(email: email, password: password)
            } else {
                print(error)
                print(error?.localizedDescription)
            }
        }
    }
    
    
    @IBAction func phoneSignIn(_ sender: Any) {
        
        guard let phoneNumber = phoneOu.text else {return}
        
        
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) {(verificationId, error) in
            if error == nil {
                //Do something
                print("VERIFICATION ID",verificationId)
                
                guard let verifyId = verificationId else {return}
                self.userDefault.set(verifyId, forKey: "verificationId")
                self.userDefault.synchronize()
                
            } else {
                print("Unable to get secret verification code from firebase", error?.localizedDescription)
            }
        }
    }
    
    
    @IBAction func verifyOPTPressed(_ sender: Any) {
        
        guard let OTPcode = otpOu.text else {return}
        
        guard let verificationId = userDefault.string(forKey: "verificationId") else {return}
        
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationId, verificationCode: OTPcode)
        
        Auth.auth().signInAndRetrieveData(with: credential) {(success, error) in
            if error == nil {
                print(success)
                print("User Signed In")
                //Perform segue
                
            }else {
                print("Something went wrong..\(error?.localizedDescription)")
            }
            
        }
    }
    
}


//
//  SignInViewController.swift
//  FireAuth
//
//  Created by Faique Ali on 27/08/2020.
//  Copyright © 2020 Faique Ali. All rights reserved.
//

import UIKit
import Firebase

class SignInViewController: UIViewController {

    //Outlets
    @IBOutlet weak var emailOu: UILabel!
    
    //Variables
    
    //Constants
    let userDefault = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        //Firebase func to return user email
        guard let email = Auth.auth().currentUser?.email else {return}
        emailOu.text = email
    }
    
    @IBAction func signOutPressed(_ sender: Any) {
        do {
            try Auth.auth().signOut()
            
            //removing again the key to track user signed out
            userDefault.removeObject(forKey: "usersignedin")
            userDefault.synchronize()
            
            //exiting VC
            self.dismiss(animated: true, completion: nil)
            
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    

}
